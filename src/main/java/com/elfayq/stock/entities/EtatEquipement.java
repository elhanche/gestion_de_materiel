package com.elfayq.stock.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class EtatEquipement implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long EtatID;
	private String EtatNom;
	
	
	
	@OneToMany(mappedBy = "etatEquipements")
	private List<Equipement> equipements;



	public Long getEtatID() {
		return EtatID;
	}



	public void setEtatID(Long etatID) {
		EtatID = etatID;
	}



	public String getEtatNom() {
		return EtatNom;
	}



	public void setEtatNom(String etatNom) {
		EtatNom = etatNom;
	}



	public List<Equipement> getEquipements() {
		return equipements;
	}



	public void setEquipements(List<Equipement> equipements) {
		this.equipements = equipements;
	}



	public EtatEquipement() {
		
	}



	
	

}
