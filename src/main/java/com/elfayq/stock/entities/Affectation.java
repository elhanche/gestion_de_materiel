package com.elfayq.stock.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
public class Affectation implements Serializable {
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idAffectation;
	
	
	
	private String code;
	
	
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateAffectationEmployer;
	
	
	@OneToOne
	@JoinColumn(name="equipID")
	private Equipement equipements;
	
	
	@ManyToOne
	@JoinColumn(name="EmpID")
	private Employer employers;
	
	
	@ManyToOne
	@JoinColumn(name="LocalID")
	private Local locals;
	
	
	@OneToMany(mappedBy="affectations")
	List<LigneAffectation> ligneAffectations;

	
	
	
	public Affectation() {
		
	}

	public long getIdAffectation() {
		return idAffectation;
	}

	public void setIdAffectation(long idAffectation) {
		this.idAffectation = idAffectation;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}



	public Date getDateAffectationEmployer() {
		return dateAffectationEmployer;
	}

	public void setDateAffectationEmployer(Date dateAffectationEmployer) {
		this.dateAffectationEmployer = dateAffectationEmployer;
	}

	public Employer getEmployers() {
		return employers;
	}

	public void setEmployers(Employer employers) {
		this.employers = employers;
	}

	
	public List<LigneAffectation> getLigneAffectations() {
		return ligneAffectations;
	}

	public void setLigneAffectations(List<LigneAffectation> ligneAffectations) {
		this.ligneAffectations = ligneAffectations;
	}

	public Local getLocals() {
		return locals;
	}

	public void setLocals(Local locals) {
		this.locals = locals;
	}



	public Equipement getEquipements() {
		return equipements;
	}

	public void setEquipements(Equipement equipements) {
		this.equipements = equipements;
	}
	
	
	
	
	
	

}
