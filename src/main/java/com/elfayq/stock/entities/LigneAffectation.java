package com.elfayq.stock.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
public class LigneAffectation implements Serializable {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long LigneAffectationID;
	
	private int quantity;
	
	@ManyToOne
	@JoinColumn(name="idAffectation")
	private Affectation affectations;
	
	@ManyToOne
	@JoinColumn(name="equipID")
	private Equipement equipements;
	
	@ManyToOne
	@JoinColumn(name="LocalID")
	private Local locals;

	public LigneAffectation() {
		
	}

	public Long getLigneAffectationID() {
		return LigneAffectationID;
	}

	public void setLigneAffectationID(Long ligneAffectationID) {
		LigneAffectationID = ligneAffectationID;
	}

	@JsonIgnore
	public Affectation getAffectations() {
		return affectations;
	}

	public void setAffectations(Affectation affectations) {
		this.affectations = affectations;
	}

	@JsonIgnore
	public Equipement getEquipements() {
		return equipements;
	}

	public void setEquipements(Equipement equipements) {
		this.equipements = equipements;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@JsonIgnore
	public Local getLocals() {
		return locals;
	}

	public void setLocals(Local locals) {
		this.locals = locals;
	}
	
	
	
	
	

}
