package com.elfayq.stock.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Local implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long LocalID;
	private String LocalNom;
	
	@ManyToOne
	@JoinColumn(name="VilleID")
	private Ville villes;
	
	
	public Long getLocalID() {
		return LocalID;
	}
	public void setLocalID(Long localID) {
		LocalID = localID;
	}
	public String getLocalNom() {
		return LocalNom;
	}
	public void setLocalNom(String localNom) {
		LocalNom = localNom;
	}
	
	
	
	public Ville getVilles() {
		return villes;
	}
	public void setVilles(Ville villes) {
		this.villes = villes;
	}
	public Local() {
		
	}
	
	
	

}
