package com.elfayq.stock.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Marque implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long MarqID;
	private String MarqNom;
	
	
	
	@OneToMany(mappedBy = "marques")
	private List<Equipement> equipements;



	public Long getMarqID() {
		return MarqID;
	}



	public void setMarqID(Long marqID) {
		MarqID = marqID;
	}



	public String getMarqNom() {
		return MarqNom;
	}



	public void setMarqNom(String marqNom) {
		MarqNom = marqNom;
	}



	public List<Equipement> getEquipements() {
		return equipements;
	}



	public void setEquipements(List<Equipement> equipements) {
		this.equipements = equipements;
	}



	public Marque() {
		
	}
	
	
	

}
