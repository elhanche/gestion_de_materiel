package com.elfayq.stock.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Ville implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long VilleID;
	private String VilleNom;
	
	
	public Long getVilleID() {
		return VilleID;
	}
	public void setVilleID(Long villeID) {
		VilleID = villeID;
	}
	public String getVilleNom() {
		return VilleNom;
	}
	public void setVilleNom(String villeNom) {
		VilleNom = villeNom;
	}
	public Ville() {
		
	}
	
	
	

}
