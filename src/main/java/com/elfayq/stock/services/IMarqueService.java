package com.elfayq.stock.services;

import java.util.List;

import com.elfayq.stock.entities.Marque;

public interface IMarqueService {
	
	
	public Marque save(Marque entity);

	public Marque update(Marque entity);

	public List<Marque> selectAll();

	public List<Marque> selectAll(String sortField, String sort);

	public void remove(Long id);

	public Marque getById(Long id);

	public Marque findOne(String paramName, Object paramValue);

	public Marque findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
