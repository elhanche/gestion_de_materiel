package com.elfayq.stock.services;

import java.util.List;

import com.elfayq.stock.entities.LigneAffectation;

public interface ILigneAffectationService {
	
	
	public LigneAffectation save(LigneAffectation entity);

	public LigneAffectation update(LigneAffectation entity);

	public List<LigneAffectation> selectAll();

	public List<LigneAffectation> selectAll(String sortField, String sort);

	public void remove(Long id);

	public LigneAffectation getById(Long id);

	public LigneAffectation findOne(String paramName, Object paramValue);

	public LigneAffectation findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
