package com.elfayq.stock.services;

import java.util.List;

import com.elfayq.stock.entities.MouvementSock;

public interface IMouvementSockService {
	
	
	public MouvementSock save(MouvementSock entity);

	public MouvementSock update(MouvementSock entity);

	public List<MouvementSock> selectAll();

	public List<MouvementSock> selectAll(String sortField, String sort);

	public void remove(Long id);

	public MouvementSock getById(Long id);

	public MouvementSock findOne(String paramName, Object paramValue);

	public MouvementSock findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);


}
