package com.elfayq.stock.services;

import java.util.List;

import com.elfayq.stock.entities.Service;

public interface IServiceService {
	
	
	public Service save(Service entity);

	public Service update(Service entity);

	public List<Service> selectAll();

	public List<Service> selectAll(String sortField, String sort);

	public void remove(Long id);

	public Service getById(Long id);

	public Service findOne(String paramName, Object paramValue);

	public Service findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
