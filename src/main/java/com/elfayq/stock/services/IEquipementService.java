package com.elfayq.stock.services;

import java.util.List;

import com.elfayq.stock.entities.Equipement;

public interface IEquipementService {

	public Equipement save(Equipement entity);

	public Equipement update(Equipement entity);

	public List<Equipement> selectAll();

	public List<Equipement> selectAll(String sortField, String sort);

	public void remove(Long id);

	public Equipement getById(Long id);

	public Equipement findOne(String paramName, Object paramValue);

	public Equipement findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
