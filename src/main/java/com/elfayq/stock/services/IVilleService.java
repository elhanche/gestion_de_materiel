package com.elfayq.stock.services;

import java.util.List;

import com.elfayq.stock.entities.Ville;

public interface IVilleService {
	
	
	public Ville save(Ville entity);

	public Ville update(Ville entity);

	public List<Ville> selectAll();

	public List<Ville> selectAll(String sortField, String sort);

	public void remove(Long id);

	public Ville getById(Long id);

	public Ville findOne(String paramName, Object paramValue);

	public Ville findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
