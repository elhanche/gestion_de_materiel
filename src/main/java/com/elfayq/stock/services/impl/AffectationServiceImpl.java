package com.elfayq.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.elfayq.stock.dao.IAffectationDao;
import com.elfayq.stock.entities.Affectation;
import com.elfayq.stock.services.IAffectationService;

@Transactional
public class AffectationServiceImpl implements IAffectationService {

	private IAffectationDao affecDao;

	

	public void setAffecDao(IAffectationDao affecDao) {
		this.affecDao = affecDao;
	}

	@Override
	public Affectation save(Affectation entity) {
		
		return affecDao.save(entity);
	}

	@Override
	public Affectation update(Affectation entity) {
		
		return affecDao.update(entity);
	}

	@Override
	public List<Affectation> selectAll() {
		
		return affecDao.selectAll();
	}

	@Override
	public List<Affectation> selectAll(String sortField, String sort) {
		
		return affecDao.selectAll(sortField, sort);
	}

	@Override
	public void remove(Long id) {
		affecDao.remove(id);
		
	}

	@Override
	public Affectation getById(Long id) {
		
		return affecDao.getById(id);
	}

	@Override
	public Affectation findOne(String paramName, Object paramValue) {
		
		return affecDao.findOne(paramName, paramValue);
	}

	@Override
	public Affectation findOne(String[] paramNames, Object[] paramValues) {
		
		return affecDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return affecDao.findCountBy(paramName, paramValue);
	}
	
	
	
	
	
	
}
