package com.elfayq.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.elfayq.stock.dao.IUtilisateurDao;
import com.elfayq.stock.entities.Utilisateur;
import com.elfayq.stock.services.IUtilisateurService;

@Transactional
public class UtilisateurServiceImpl implements IUtilisateurService{
	
	private IUtilisateurDao utilisDao;

	public void setUtilisDao(IUtilisateurDao utilisDao) {
		this.utilisDao = utilisDao;
	}

	@Override
	public Utilisateur save(Utilisateur entity) {
		
		return utilisDao.save(entity);
	}

	@Override
	public Utilisateur update(Utilisateur entity) {
		
		return utilisDao.update(entity);
	}

	@Override
	public List<Utilisateur> selectAll() {
		
		return utilisDao.selectAll();
	}

	@Override
	public List<Utilisateur> selectAll(String sortField, String sort) {
		
		return utilisDao.selectAll(sortField, sort);
	}

	@Override
	public void remove(Long id) {
		utilisDao.remove(id);
		
	}

	@Override
	public Utilisateur getById(Long id) {
		
		return utilisDao.getById(id);
	}

	@Override
	public Utilisateur findOne(String paramName, Object paramValue) {
		
		return utilisDao.findOne(paramName, paramValue);
	}

	@Override
	public Utilisateur findOne(String[] paramNames, Object[] paramValues) {
		
		return utilisDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return utilisDao.findCountBy(paramName, paramValue);
	}
	
	
	

}
