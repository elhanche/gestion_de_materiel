package com.elfayq.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.elfayq.stock.dao.IEquipementDao;
import com.elfayq.stock.dao.impl.EquipementDaoImpl;
import com.elfayq.stock.entities.Equipement;
import com.elfayq.stock.services.IEquipementService;
//L'annotation transactionnel pour dire que la classe contient des m�thode de transaction avec la base de donn�es
@Transactional
public class EquipementServiceImpl implements IEquipementService{

	private IEquipementDao equipementsDao;

	
	
	public void setEquipementsDao(IEquipementDao equipementsDao) {
		this.equipementsDao = equipementsDao;
	}
	
	

	@Override
	public Equipement save(Equipement entity) {
		return equipementsDao.save(entity);
	}

	@Override
	public Equipement update(Equipement entity) {
		
		return equipementsDao.save(entity);
	}

	@Override
	public List<Equipement> selectAll() {
		
		return equipementsDao.selectAll();
	}

	@Override
	public List<Equipement> selectAll(String sortField, String sort) {
		
		return equipementsDao.selectAll(sortField, sort);
	}

	@Override
	public void remove(Long id) {
		equipementsDao.remove(id);
		
	}

	@Override
	public Equipement getById(Long id) {
		
		return equipementsDao.getById(id);
	}

	@Override
	public Equipement findOne(String paramName, Object paramValue) {
		
		return equipementsDao.findOne(paramName, paramValue);
	}

	@Override
	public Equipement findOne(String[] paramNames, Object[] paramValues) {
		
		return equipementsDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return equipementsDao.findCountBy(paramName, paramValue);
	}
	
	
	
}
