package com.elfayq.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.elfayq.stock.dao.ILigneAffectationDao;
import com.elfayq.stock.entities.LigneAffectation;
import com.elfayq.stock.services.ILigneAffectationService;


@Transactional
public class LigneAffectationServiceImpl implements ILigneAffectationService {
	
	private ILigneAffectationDao ligneAffecDao;

	public void setLigneAffecDao(ILigneAffectationDao ligneAffecDao) {
		this.ligneAffecDao = ligneAffecDao;
	}

	@Override
	public LigneAffectation save(LigneAffectation entity) {
		
		return ligneAffecDao.save(entity);
	}

	@Override
	public LigneAffectation update(LigneAffectation entity) {
		
		return ligneAffecDao.update(entity);
	}

	@Override
	public List<LigneAffectation> selectAll() {
		
		return ligneAffecDao.selectAll();
	}

	@Override
	public List<LigneAffectation> selectAll(String sortField, String sort) {
		
		return ligneAffecDao.selectAll(sortField, sort);
	}

	@Override
	public void remove(Long id) {
		ligneAffecDao.remove(id);
		
	}

	@Override
	public LigneAffectation getById(Long id) {
		
		return ligneAffecDao.getById(id);
	}

	@Override
	public LigneAffectation findOne(String paramName, Object paramValue) {
		
		return ligneAffecDao.findOne(paramName, paramValue);
	}

	@Override
	public LigneAffectation findOne(String[] paramNames, Object[] paramValues) {
		
		return ligneAffecDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return ligneAffecDao.findCountBy(paramName, paramValue);
	}
	
	
	

}
