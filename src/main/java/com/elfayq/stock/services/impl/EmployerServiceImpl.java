package com.elfayq.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.elfayq.stock.dao.IEmployerDao;
import com.elfayq.stock.entities.Employer;
import com.elfayq.stock.services.IEmployerService;


@Transactional
public class EmployerServiceImpl implements IEmployerService {
	
	private IEmployerDao emplDao;

	public void setEmplDao(IEmployerDao emplDao) {
		this.emplDao = emplDao;
	}

	@Override
	public Employer save(Employer entity) {
		
		return emplDao.save(entity);
	}

	@Override
	public Employer update(Employer entity) {
		
		return emplDao.update(entity);
	}

	@Override
	public List<Employer> selectAll() {
		
		return emplDao.selectAll();
	}

	@Override
	public List<Employer> selectAll(String sortField, String sort) {
		
		return emplDao.selectAll(sortField, sort);
	}

	@Override
	public void remove(Long id) {
		emplDao.remove(id);
		
	}

	@Override
	public Employer getById(Long id) {
		
		return emplDao.getById(id);
	}

	@Override
	public Employer findOne(String paramName, Object paramValue) {
		
		return emplDao.findOne(paramName, paramValue);
	}

	@Override
	public Employer findOne(String[] paramNames, Object[] paramValues) {
		
		return emplDao.findOne(paramNames, paramNames);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return emplDao.findCountBy(paramName, paramValue);
	}
	
	

}
