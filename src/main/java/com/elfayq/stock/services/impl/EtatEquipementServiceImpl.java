package com.elfayq.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.elfayq.stock.dao.IEtatEquipementDao;
import com.elfayq.stock.entities.EtatEquipement;
import com.elfayq.stock.services.IEtatEquipementService;

@Transactional
public class EtatEquipementServiceImpl implements IEtatEquipementService {
	
	private IEtatEquipementDao etatEquipementDao;

	public void setEtatEquipementDao(IEtatEquipementDao etatEquipementDao) {
		this.etatEquipementDao = etatEquipementDao;
	}

	@Override
	public EtatEquipement save(EtatEquipement entity) {
		
		return etatEquipementDao.save(entity);
	}

	@Override
	public EtatEquipement update(EtatEquipement entity) {
		
		return etatEquipementDao.update(entity);
	}

	@Override
	public List<EtatEquipement> selectAll() {
		return etatEquipementDao.selectAll();
	}

	@Override
	public List<EtatEquipement> selectAll(String sortField, String sort) {
		
		return etatEquipementDao.selectAll(sortField, sort);
	}

	@Override
	public void remove(Long id) {
		etatEquipementDao.remove(id);
		
	}

	@Override
	public EtatEquipement getById(Long id) {
		
		return etatEquipementDao.getById(id);
	}

	@Override
	public EtatEquipement findOne(String paramName, Object paramValue) {
		
		return etatEquipementDao.findOne(paramName, paramValue);
	}

	@Override
	public EtatEquipement findOne(String[] paramNames, Object[] paramValues) {
		
		return etatEquipementDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return etatEquipementDao.findCountBy(paramName, paramValue);
	}
	
	
	

}
