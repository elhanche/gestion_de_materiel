package com.elfayq.stock.services;

import java.util.List;

import com.elfayq.stock.entities.EtatEquipement;

public interface IEtatEquipementService {
	
	
	public EtatEquipement save(EtatEquipement entity);

	public EtatEquipement update(EtatEquipement entity);

	public List<EtatEquipement> selectAll();

	public List<EtatEquipement> selectAll(String sortField, String sort);

	public void remove(Long id);

	public EtatEquipement getById(Long id);

	public EtatEquipement findOne(String paramName, Object paramValue);

	public EtatEquipement findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
