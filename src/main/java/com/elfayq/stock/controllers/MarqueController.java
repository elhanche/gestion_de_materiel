package com.elfayq.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.elfayq.stock.entities.Marque;
import com.elfayq.stock.services.IMarqueService;


@Controller
@RequestMapping(value = "/marque")
public class MarqueController {
	
	@Autowired	
	private IMarqueService catMarque;
	
	
	
	@RequestMapping(value = "/")
	public String marque(Model model) {

		  List<Marque> marques = catMarque.selectAll();
		  
		
		if (marques == null) {
			
			marques = new ArrayList<Marque>();
			
		}
	   model.addAttribute("marques",marques);
	    
	    
		return "marque/marque";
		
		
	}
	
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterMarque(Model model) {
		
		Marque marque = new Marque();
		

		model.addAttribute("marque", marque);
	
		
		return "marque/ajoutermarque";
		
	}
	
	
	
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerMarque(Model model, Marque marque ) {
		
	if (marque.getMarqID()!= null) {
		catMarque.update(marque);
			
		} else {
			catMarque.save(marque);
		}
		
	
		return "redirect:/marque/";
	}
	
	
	@RequestMapping(value = "/modifier/{MarqID}")
	public String modifierMarque(Model model, @PathVariable Long MarqID) {
		
		if (MarqID != null) {
			
			Marque marque = catMarque.getById(MarqID); 
			
			if(marque != null) {
				 model.addAttribute("marque",marque);
			}
		}
		
	
		return "marque/ajoutermarque";
	}
	
	
	
	@RequestMapping(value = "/supprimer/{MarqID}")
	public String supprimerMarque(Model model, @PathVariable Long MarqID) {
		
		if (MarqID != null) {
			
			Marque marque = catMarque.getById(MarqID);
			if(marque != null) {
				catMarque.remove(MarqID);
			}
		}
		
	
		return "redirect:/marque/";
	}
	
	
	

}
