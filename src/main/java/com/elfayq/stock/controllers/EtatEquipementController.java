package com.elfayq.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.elfayq.stock.entities.EtatEquipement;
import com.elfayq.stock.services.IEtatEquipementService;


@Controller
@RequestMapping(value = "/etatequipement")
public class EtatEquipementController {
	
	@Autowired	
	private IEtatEquipementService catEtat;
	
	
	
	@RequestMapping(value = "/")
	public String etatequipement(Model model) {

		  List<EtatEquipement> etatequipements = catEtat.selectAll();
		  
		
		if (etatequipements == null) {
			
			etatequipements = new ArrayList<EtatEquipement>();
			
		}
	   model.addAttribute("etatequipements",etatequipements);
	    
	    
		return "etatequipement/etatequipement";
		
		
	}
	
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterEtatequipement(Model model) {
		
		EtatEquipement etatequipement = new EtatEquipement();
		

		model.addAttribute("etatequipement", etatequipement);
	
		
		return "etatequipement/ajouteretatequipement";
		
	}
	
	
	
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerEtatequipement(Model model, EtatEquipement etatequipement ) {
		
	if (etatequipement.getEtatID()!= null) {
		catEtat.update(etatequipement);
			
		} else {
			catEtat.save(etatequipement);
		}
		
	
		return "redirect:/etatequipement/";
	}
	
	
	@RequestMapping(value = "/modifier/{EtatID}")
	public String modifierEtatequipement(Model model, @PathVariable Long EtatID) {
		
		if (EtatID != null) {
			
			EtatEquipement etatequipement = catEtat.getById(EtatID); 
			
			if(etatequipement != null) {
				 model.addAttribute("etatequipement",etatequipement);
			}
		}
		
	
		return "etatequipement/ajouteretatequipement";
	}
	
	
	
	@RequestMapping(value = "/supprimer/{EtatID}")
	public String supprimerEtatequipement(Model model, @PathVariable Long EtatID) {
		
		if (EtatID != null) {
			
			EtatEquipement etatequipement = catEtat.getById(EtatID);
			if(etatequipement != null) {
				catEtat.remove(EtatID);
			}
		}
		
	
		return "redirect:/etatequipement/";
	}
	
	
	

}
