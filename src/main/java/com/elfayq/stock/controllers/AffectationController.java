package com.elfayq.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.elfayq.stock.entities.Affectation;
import com.elfayq.stock.entities.LigneAffectation;
import com.elfayq.stock.services.IAffectationService;
import com.elfayq.stock.services.IEmployerService;
import com.elfayq.stock.services.IEquipementService;
import com.elfayq.stock.services.ILigneAffectationService;
import com.elfayq.stock.services.ILocalService;

@Controller
@RequestMapping(value = "/affectation")
public class AffectationController {
	
	
	@Autowired
	private IAffectationService affectationService;
	
	@Autowired
	private ILigneAffectationService ligneAffecService;
	
	@Autowired
	private IEquipementService equipementService;
	
	@Autowired
	private IEmployerService employerService;
	
	@Autowired ILocalService localService; 
	
	
	@RequestMapping(value = "/")
	public String index(Model model) {
		
		List<Affectation> affectations =  affectationService.selectAll();
		if (affectations.isEmpty()) {
			affectations = new ArrayList<Affectation>();
		} 
		
		model.addAttribute("affectations", affectations);
		return "affectation/affectation";
	}

}
