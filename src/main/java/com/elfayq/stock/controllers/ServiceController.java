package com.elfayq.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.elfayq.stock.entities.Service;
import com.elfayq.stock.services.IServiceService;

@Controller
@RequestMapping(value = "/service")
public class ServiceController {
	
	@Autowired
	private IServiceService serviceService;
	
	
	
	@RequestMapping(value = "/")
	public String service(Model model) {

		  List<Service> services = serviceService.selectAll();
		  
		
		if (services == null) {
			
			services = new ArrayList<Service>();
			
		}
	   model.addAttribute("services",services);
	    
	    
		return "service/service";
		
		
	}
	
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterService(Model model) {
		
		Service service = new Service();
		

		model.addAttribute("service", service);
	
		
		return "service/ajouterservice";
		
	}
	
	
	
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerService(Model model, Service service ) {
		
	if (service.getServID()!= null) {
		serviceService.update(service);
			
		} else {
			serviceService.save(service);
		}
		
	
		return "redirect:/service/";
	}
	
	
	@RequestMapping(value = "/modifier/{ServID}")
	public String modifierService(Model model, @PathVariable Long ServID) {
		
		if (ServID != null) {
			
			Service service = serviceService.getById(ServID); 
			
			if(service != null) {
				 model.addAttribute("service",service);
			}
		}
		
	
		return "service/ajouterservice";
	}
	
	
	
	@RequestMapping(value = "/supprimer/{ServID}")
	public String supprimerService(Model model, @PathVariable Long ServID) {
		
		if (ServID != null) {
			
			Service service = serviceService.getById(ServID);
			if(service != null) {
				serviceService.remove(ServID);
			}
		}
		
	
		return "redirect:/service/";
	}
	
	
	

}
