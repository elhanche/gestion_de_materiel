package com.elfayq.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.elfayq.stock.entities.Ville;
import com.elfayq.stock.services.IVilleService;

@Controller
@RequestMapping(value = "/ville")
public class VilleController {
	
	@Autowired
	private IVilleService villeService;
	
	
	
	@RequestMapping(value = "/")
	public String ville(Model model) {

		  List<Ville> villes = villeService.selectAll();
		  
		
		if (villes == null) {
			
			villes = new ArrayList<Ville>();
			
		}
	   model.addAttribute("villes",villes);
	    
	    
		return "ville/ville";
		
		
	}
	
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterVille(Model model) {
		
		Ville ville = new Ville();
		

		model.addAttribute("ville", ville);
	
		
		return "ville/ajouterville";
		
	}
	
	
	
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerVille(Model model, Ville ville ) {
		
	if (ville.getVilleID()!= null) {
		villeService.update(ville);
			
		} else {
			villeService.save(ville);
		}
		
	
		return "redirect:/ville/";
	}
	
	
	@RequestMapping(value = "/modifier/{VilleID}")
	public String modifierVille(Model model, @PathVariable Long VilleID) {
		
		if (VilleID != null) {
			
			Ville ville = villeService.getById(VilleID); 
			
			if(ville != null) {
				 model.addAttribute("ville",ville);
			}
		}
		
	
		return "ville/ajouterville";
	}
	
	
	
	@RequestMapping(value = "/supprimer/{VilleID}")
	public String supprimerVille(Model model, @PathVariable Long VilleID) {
		
		if (VilleID != null) {
			
			Ville ville = villeService.getById(VilleID);
			if(ville != null) {
				villeService.remove(VilleID);
			}
		}
		
	
		return "redirect:/ville/";
	}
	
	
	

}
