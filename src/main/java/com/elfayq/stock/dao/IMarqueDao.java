package com.elfayq.stock.dao;

import com.elfayq.stock.entities.Marque;

public interface IMarqueDao extends IGenericDao<Marque> {

}
