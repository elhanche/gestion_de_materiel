package com.elfayq.stock.dao;

import com.elfayq.stock.entities.EtatEquipement;

public interface IEtatEquipementDao extends IGenericDao<EtatEquipement> {

}
