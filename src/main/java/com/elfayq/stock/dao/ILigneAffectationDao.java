package com.elfayq.stock.dao;

import com.elfayq.stock.entities.LigneAffectation;

public interface ILigneAffectationDao extends IGenericDao<LigneAffectation> {

}
