package com.elfayq.stock.dao;

import com.elfayq.stock.entities.Affectation;

public interface IAffectationDao extends IGenericDao<Affectation> {

}
