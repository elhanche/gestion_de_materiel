package com.elfayq.stock.dao;

import com.elfayq.stock.entities.Employer;

public interface IEmployerDao extends IGenericDao<Employer> {

}
