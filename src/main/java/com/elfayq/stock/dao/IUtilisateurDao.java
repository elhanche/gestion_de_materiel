package com.elfayq.stock.dao;

import com.elfayq.stock.entities.Utilisateur;

public interface IUtilisateurDao extends IGenericDao<Utilisateur> {

}
