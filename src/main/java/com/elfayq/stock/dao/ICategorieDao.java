package com.elfayq.stock.dao;

import com.elfayq.stock.entities.Categorie;

public interface ICategorieDao extends IGenericDao<Categorie>{

}
