package com.elfayq.stock.dao;

import com.elfayq.stock.entities.Local;

public interface ILocalDao extends IGenericDao<Local> {

}
